model MudMountainReservoirOptimization
  extends MudMountainReservoir;

// variable used to represent the absolute value of the derivative of Qturbine, used to smoothen
  //    operation of the turbine.
  Real ReservoirMudMountain_absQturbder(min = 0);
  Real ReservoirMudMountain_absQspillder(min = 0);

    end MudMountainReservoirOptimization;
