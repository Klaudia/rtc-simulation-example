model MudMountainReservoirSimulation
  extends MudMountainReservoir;

  input SI.VolumeFlowRate ReservoirMudMountain_Qbottomoutlet(fixed = false);  
  input SI.VolumeFlowRate ReservoirMudMountain_V_user(fixed = true);

    end MudMountainReservoirSimulation;
