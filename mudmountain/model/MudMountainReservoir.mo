model MudMountainReservoir
  import SI = Modelica.SIunits;
  // Schematization elements
  Deltares.ChannelFlow.SimpleRouting.Reservoir.Reservoir ReservoirMudMountain annotation(
    Placement(visible = true, transformation(origin = {-42, 22}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Deltares.ChannelFlow.SimpleRouting.BoundaryConditions.Inflow InflowMudMountain annotation(
    Placement(visible = true, transformation(origin = {-80, 22}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Deltares.ChannelFlow.SimpleRouting.BoundaryConditions.Terminal TerminalDownstream annotation(
    Placement(visible = true, transformation(origin = {30, 22}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

  // Time series
  input SI.VolumeFlowRate ReservoirMudMountain_Inflow_Q(fixed = true);
  input SI.VolumeFlowRate ReservoirMudMountain_Qturbine(fixed = false);
  input SI.VolumeFlowRate ReservoirMudMountain_Qspill(fixed = false);  
  output SI.Volume  ReservoirMudMountain_V(min = 10^5, max = 1.68*10^8);
  output SI.VolumeFlowRate ReservoirMudMountain_Qout(min = 0, max = 8*10^3);
  output SI.VolumeFlowRate Downstream_Q(min = 0, max = 3*10^4);
equation
  connect(ReservoirMudMountain.QOut, TerminalDownstream.QIn) annotation(
    Line(points = {{-34, 22}, {30, 22}}));
  connect(InflowMudMountain.QOut, ReservoirMudMountain.QIn) annotation(
    Line(points = {{-72, 22}, {-50, 22}, {-50, 22}, {-50, 22}}));
  InflowMudMountain.Q = ReservoirMudMountain_Inflow_Q;
  ReservoirMudMountain.Q_spill = ReservoirMudMountain_Qspill;
  ReservoirMudMountain.Q_turbine = ReservoirMudMountain_Qturbine;
  ReservoirMudMountain_V = ReservoirMudMountain.V;
  ReservoirMudMountain_Qout = ReservoirMudMountain.QOut.Q;
  Downstream_Q = TerminalDownstream.QIn.Q;
    end MudMountainReservoir;
