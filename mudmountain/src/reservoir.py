import numpy as np
from typing import Union
import logging
import os

logger = logging.getLogger("rtctools")

class Reservoir:

    def __init__(self, name, vh_data, reservoir_properties, fews_location=None, fews_qualifier=None):
        self.__vh_lookup = vh_data
        self.name = name
        if fews_location is not None:
           self.fews_location=fews_location
        else: self.fews_location = name
        if fews_qualifier is not None:
            self.fews_qualifier = fews_qualifier
        else: self.fews_qualifier = None
        if reservoir_properties['Unit'].upper() == 'SI':
            self.properties = reservoir_properties
        elif reservoir_properties['Unit'].upper()== 'US':
            reservoir_properties['surcharge'] = reservoir_properties['surcharge']*0.3048
            reservoir_properties['fullsupply'] = reservoir_properties['fullsupply']*0.3048
            reservoir_properties['crestheight'] = reservoir_properties['crestheight']*0.3048
            reservoir_properties['volume_min'] = reservoir_properties['volume_min']*1233481.8375
            reservoir_properties['volume_max'] = reservoir_properties['volume_max']*1233481.8375
            reservoir_properties['q_turbine_max'] = reservoir_properties['q_turbine_max']*0.028316847
            reservoir_properties['q_spill_max'] = reservoir_properties['q_spill_max']*0.028316847
            reservoir_properties['q_bottomoutlet_max'] = reservoir_properties['q_bottomoutlet_max']*0.028316847


            self.properties = reservoir_properties
        else:
            self.properties = reservoir_properties
            logger.warning('Units should be SI or US, SI is taken as default')
        self.volume_setpoints = {}

    def level_to_volume(self, levels: Union[float, np.ndarray]):
        try:
            return  np.interp(levels, self.__vh_lookup['Elevation_m'], self.__vh_lookup['Storage_m3'])
        except:
            return np.interp(levels, self.__vh_lookup['Elevation_ft']*0.3048, self.__vh_lookup['Storage_kaf']*1233481.8375)

    def volume_to_level(self, volumes: Union[float, np.ndarray]):
        try:
            return np.interp(volumes, self.__vh_lookup['Storage_m3'], self.__vh_lookup['Elevation_m'])
        except:
            return np.interp(volumes, self.__vh_lookup['Storage_kaf']*1233481.8375, self.__vh_lookup['Elevation_ft']*0.3048)

    def set_volume_setpoints(self):
        # define interpolated volume setpoints corresponding to heights used in goals:
        # Note: these property names are hardcoded
        self.volume_setpoints['surcharge'] = self.level_to_volume(self.properties['surcharge'])
        self.volume_setpoints['fullsupply'] = self.level_to_volume(self.properties['fullsupply'])
        self.volume_setpoints['crestheight'] = self.level_to_volume(self.properties['crestheight'])
