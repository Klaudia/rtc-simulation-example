import logging
from abc import abstractmethod
from typing import Dict
import pandas4RTC
from rtctools.simulation.simulation_problem import SimulationProblem
from rtctools.simulation.pi_mixin import PIMixin
from reservoir import Reservoir

logger = logging.getLogger("rtctools")


class ReservoirSimulationMixin(PIMixin, SimulationProblem):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.newtime = None
        self.plot_reservoirs = True

    @property
    @abstractmethod
    def reservoirs(self) -> Dict[str, Reservoir]:
        return {}

    def get_output_variables(self):
        output_variables = super().get_output_variables().copy()

        for res in self.reservoirs:
            output_variables.extend(['{}_V'.format(res), '{}_Qspill'.format(res), '{}_Qturbine'.format(res)])

        return output_variables

    def pre(self):
        super().pre()
        parameters = self.parameters()
        for res in self.reservoirs:

            if not parameters['{}.n_QForcing'.format(res)] == 0:
                logger.error('There cannot be any forcing on the reservoir itself.')
            if not parameters['{}.n_QLateral'.format(res)] == 0:
                logger.error('There cannot be any laterals on the reservoir itself.')

    def select_operation_mode(self, res):
        import pandas as pd
        from pandas4RTC import PIXML_2_DF
        import os

        if os.path.isfile(os.path.join(self._input_folder, 'timeseries_mods.xml')):
            print("File exist")
            modifier_time_series = PIXML_2_DF('../input/timeseries_mods.xml', rtcDataConfig='default',
                                              multi_index_cols=False,
                                              squeezeQualifiers=True)
            # Todo: make the case when there is no qualifier
            if self.reservoirs[res].fews_qualifier is not None:
                operation_mode_time_series = modifier_time_series[
                    self.reservoirs[res].fews_location + ':RTC2_Option:' + self.reservoirs[res].fews_qualifier]
            else:
                operation_mode_time_series = modifier_time_series[self.reservoirs[res].fews_location + ':RTC2_Option'
                                                                  ]

            current_time = self.get_current_time()
            simulation_start_time = modifier_time_series.index[0]
            current_time_in_pandas = simulation_start_time + pd.Timedelta(seconds=current_time)
            reservoir_operation_mode = operation_mode_time_series.loc[current_time_in_pandas]
        else:
            print("File not exist")
            operation_strategy_chosen = 0

            parameters = self.parameters()
            if parameters.get("{}_passInflow".format(res), False):
                reservoir_operation_mode = 0
                operation_strategy_chosen += 1

            elif parameters.get("{}_userRelease".format(res), False):
                reservoir_operation_mode = 2
                operation_strategy_chosen += 1

            elif parameters.get("{}_user_volume".format(res), False):
                reservoir_operation_mode = 3
                operation_strategy_chosen += 1

            elif parameters.get("{}_user_level".format(res), False):
                reservoir_operation_mode = 1
                operation_strategy_chosen += 1

            elif parameters.get("{}_constantRelease".format(res), False):
                reservoir_operation_mode = 7
                operation_strategy_chosen += 1

            elif parameters.get("{}_dH".format(res), False):
                reservoir_operation_mode = 4
                operation_strategy_chosen += 1

            elif parameters.get("{}_dQ".format(res), False):
                reservoir_operation_mode = 5
                operation_strategy_chosen += 1

            if operation_strategy_chosen < 1:
                reservoir_operation_mode = 0
                logger.warning("No inflow strategy is chosen, inflow will be passed")
            if operation_strategy_chosen > 1:
                logger.warning(
                    "More than one operation strategy is chosen, the last one will be applied. Please chose only one "
                    "for "
                    "clarity.")

        return reservoir_operation_mode

    def calculate_release_discharge(self, res):

        import pandas as pd
        import os
        from pandas4RTC import PIXML_2_DF
        modifier_time_series = PIXML_2_DF('../input/timeseries_mods.xml', rtcDataConfig='default',
                                          multi_index_cols=False,
                                          squeezeQualifiers=True)

        current_time = self.get_current_time()
        simulation_start_time = modifier_time_series.index[0]
        current_time_in_pandas = simulation_start_time + pd.Timedelta(seconds=current_time)
        current_step = int(current_time / self._IOMixin__dt)
        volume_now = self._io_output['ReservoirMudMountain.V'][current_step]

        reservoir_operation_mode = self.select_operation_mode(res)
        q_release = None
        parameters = self.parameters()

        if reservoir_operation_mode == 0:

            import numpy as np
            if np.isnan(self.timeseries_at('{}_Inflow_Q'.format(res), self.newtime)):
                logger.warning("Inflow is NaN")
                q_release = self._io_output['ReservoirMudMountain_Qturbine'][current_step]
            else:
                q_release = self.timeseries_at('{}_Inflow_Q'.format(res), self.newtime)
            logger.info("Pass inflow")

        if reservoir_operation_mode == 2:  # Q
            if os.path.isfile(os.path.join(self._input_folder, 'timeseries_mods.xml')):
                logger.info("Read data from timeseries_mods.xml")
                if self.reservoirs[res].fews_qualifier is not None:
                    q_release = modifier_time_series[
                        self.reservoirs[res].fews_location + ':SETQ_RTC2:' + self.reservoirs[res].fews_qualifier].loc[
                        current_time_in_pandas]
                else:
                    q_release = modifier_time_series[self.reservoirs[res].fews_location + ':SETQ_RTC2'].loc[
                        current_time_in_pandas]
            else:
                q_release = self.timeseries_at("{}_Release_User".format(res), self.newtime)
            logger.info("user-defined release")

        if reservoir_operation_mode == 3:  # V

            if os.path.isfile(os.path.join(self._input_folder, 'timeseries_mods.xml')):
                logger.info("Read data from timeseries_mods.xml")
                if self.reservoirs[res].fews_qualifier is not None:
                    volume_new = modifier_time_series[
                        self.reservoirs[res].fews_location + ':SETS_RTC2:' + self.reservoirs[res].fews_qualifier].loc[
                        current_time_in_pandas]
                else:
                    volume_new = modifier_time_series[self.reservoirs[res].fews_location + ':SETS_RTC2'].loc[
                        current_time_in_pandas]
            else:
                volume_new = self.timeseries_at('{}_V_user'.format(res), self.newtime)

            volume_difference = volume_new - volume_now
            release_based_on_volume = (- volume_difference) / (self.newtime - self.get_current_time())
            q_release = release_based_on_volume + self.timeseries_at('{}_Inflow_Q'.format(res), self.newtime)

        if reservoir_operation_mode == 6:  # dS

            if os.path.isfile(os.path.join(self._input_folder, 'timeseries_mods.xml')):
                logger.info("Read data from timeseries_mods.xml")
                if self.reservoirs[res].fews_qualifier is not None:
                    volume_difference = modifier_time_series[
                        self.reservoirs[res].fews_location + ':SETDS_RTC2:' + self.reservoirs[res].fews_qualifier].loc[
                        current_time_in_pandas]
                else:
                    volume_difference = modifier_time_series[self.reservoirs[res].fews_location + ':SETDS_RTC2'].loc[
                        current_time_in_pandas]
            else:
                # Todo implement volume difference with parameters
                volume_difference = modifier_time_series['MMRW1:SETDS_RTC2:DS'].loc[current_time_in_pandas]

            release_based_on_volume = (- volume_difference) / (self.newtime - self.get_current_time())
            q_release = release_based_on_volume + self.timeseries_at('{}_Inflow_Q'.format(res), self.newtime)

        if reservoir_operation_mode == 1:  # H user
            if os.path.isfile(os.path.join(self._input_folder, 'timeseries_mods.xml')):
                logger.info("Read data from timeseries_mods.xml")
                if self.reservoirs[res].fews_qualifier is not None:
                    h_user = modifier_time_series[
                        self.reservoirs[res].fews_location + ':SETH_RTC2:' + self.reservoirs[res].fews_qualifier].loc[
                        current_time_in_pandas]
                else:
                    h_user = modifier_time_series[self.reservoirs[res].fews_location + ':SETH_RTC2'].loc[
                        current_time_in_pandas]
            else:
                h_user = self.timeseries_at('{}_H_user'.format(res), self.get_current_time())

            volume_new = self.reservoirs[res].level_to_volume(h_user)
            volume_difference = volume_new - volume_now
            release_based_on_volume = (- volume_difference) / (self.newtime - self.get_current_time())
            q_release = release_based_on_volume + self.timeseries_at('{}_Inflow_Q'.format(res), self.newtime)

        if reservoir_operation_mode == 7:
            q_release = parameters.get("{}_constantRelease_value".format(res), 9.99)
            logger.info("Constant release")

        if reservoir_operation_mode == 4:  # dH
            current_h = self.reservoirs[res].volume_to_level(volume_now)

            if os.path.isfile(os.path.join(self._input_folder, 'timeseries_mods.xml')):
                logger.info("Read data from timeseries_mods.xml")
                if self.reservoirs[res].fews_qualifier is not None:
                    delta_h_user = modifier_time_series[
                        self.reservoirs[res].fews_location + ':SETDH_RTC2:' + self.reservoirs[res].fews_qualifier].loc[
                        current_time_in_pandas]
                else:
                    delta_h_user = modifier_time_series[self.reservoirs[res].fews_location + ':SETDH_RTC2'].loc[
                        current_time_in_pandas]
                volume_new = self.reservoirs[res].level_to_volume(
                    current_h + delta_h_user)

            else:
                volume_new = self.reservoirs[res].level_to_volume(
                    current_h + self.timeseries_at('{}_dH_value'.format(res), self.get_current_time()))
            volume_difference = volume_new - volume_now
            release_based_on_volume = (- volume_difference) / (self.newtime - self.get_current_time())
            q_release = release_based_on_volume + self.timeseries_at('{}_Inflow_Q'.format(res), self.newtime)

        if reservoir_operation_mode == 5:  # dQ

            current_release = self.get_var('{}_Qturbine'.format(res)) + self.get_var('{}_Qspill'.format(res))
            if os.path.isfile(os.path.join(self._input_folder, 'timeseries_mods.xml')):
                logger.info("Read data from timeseries_mods.xml")
                if self.reservoirs[res].fews_qualifier is not None:
                    q_release = current_release + modifier_time_series[
                        self.reservoirs[res].fews_location + ':SETDQ_RTC2:' + self.reservoirs[res].fews_qualifier].loc[
                        current_time_in_pandas]
                else:
                    q_release = current_release + \
                                modifier_time_series[self.reservoirs[res].fews_location + ':SETDQ_RTC2'].loc[
                                    current_time_in_pandas]
            else:
                q_release = current_release + self.timeseries_at('{}_dQ_value'.format(res), self.get_current_time())

        return q_release

    def update(self, dt):

        # new simulation time
        if dt < 0:
            dt = self.times()[1] - self.times()[0]
        self.newtime = self.get_current_time() + dt
        logger.info(
            "*** Simulation time:{} ***".format(self.io.sec_to_datetime(self.newtime, self.io.reference_datetime)))

        for res in self.reservoirs:
            q_release = self.calculate_release_discharge(res)
            distributed_discharge = self.distribute_release(res, q_release)
            self.set_all_q_release_sorts(res, distributed_discharge)

        super().update(dt)

    def set_all_q_release_sorts(self, res, parameter_dict):
        for parameter in parameter_dict:
            self.set_var('{}_{}'.format(res, parameter), parameter_dict[parameter])

    def distribute_release(self, res, q_user):
        import numpy as np
        if np.isnan(q_user):
            logger.warning("User given discharge is not a number")
        # applies a (user-defined) release timeseries to a reservoir and distributes this release on turbine and
        q_turbine_max = self.reservoirs[res].properties['q_turbine_max']
        q_spill_max = self.reservoirs[res].properties['q_spill_max']
        q_bottomoutlet_max = self.reservoirs[res].properties['q_bottomoutlet_max']
        # Todo: handle if bott. outlet propertiy does not exist
        # Todo: implement the bottomoutlet routine, is it chosen or the spillway?

        # Turbine flow is minimum zero and maximum its capacity
        q_turbine = max(min(q_turbine_max, q_user), 0)
        # Bottomoutlet is the flow that should be released and not turbined and less than the bo. capacity
        q_bottomoutlet = min(q_bottomoutlet_max, (q_user - q_turbine))
        # if crest height allows, the rest of the flow is spilled
        # Todo: It is checked just once, and then we might spill more water than the crest height
        if self.get_var("{}_V".format(res)) > self.reservoirs[res].volume_setpoints['crestheight']:
            q_spill = min(q_spill_max, (q_user - q_turbine - q_bottomoutlet))
        else:
            q_spill = 0.0
        q_remainder_have_to_stay = q_user - q_turbine - q_spill - q_bottomoutlet
        logger.info(
            "SetRelease: {}, distributed to Q_turbine = {},  Q_spill =  {}, Q_bottomoutlet =  {}, Qremainder =  {} ("
            "not released, remains in "
            "reservoir)".format(
                q_user, q_turbine, q_spill, q_bottomoutlet, q_remainder_have_to_stay))
        q_release_sorts = dict(zip(['Qturbine', 'Qspill', 'Qbottomoutlet'], [q_turbine, q_spill, q_bottomoutlet]))
        return q_release_sorts

    def post(self):
        results = self.extract_results()

        for res in self.reservoirs:
            result_h = self.reservoirs[res].volume_to_level(results['{}_V'.format(res)])
            self.set_timeseries('{}_H'.format(res), result_h, unit='m+MSL')
        super().post()
        # This method only works with PI mixin
        extra_units = {}

        # End of water level computation
        for var in self.get_output_variables():
            if '.Q' in var or '_Q' in var:
                extra_units[var] = 'm3/s'
            elif '.V' in var or '_V' in var:
                extra_units[var] = 'm3'

        for var, unit in extra_units.items():
            for alias in self.alias_relation.aliases(var):
                self.timeseries_import.set_unit(alias, unit, 0)
        # End of unit specification

        pandas4RTC.main()

        if self.plot_reservoirs is True:
            import matplotlib.pyplot as plt
            import numpy as np
            import os
            plt.clf()
            plot_time = [number / (3600 * 24) for number in self._simulation_times]
            for res in self.reservoirs:
                plt.clf()
                fig1 = plt.figure(1)
                plt.plot(plot_time[1:], results[res + '_Qturbine'][1:], label='Q_turbine')
                plt.plot(plot_time[1:], results[res + '_Qspill'][1:], label='Q_spill')
                try:
                    plt.plot(plot_time[1:], self.get_timeseries(res + '.QIn.Q')[1:], label='QIn')
                except KeyError:
                    plt.plot(plot_time[1:], results[res + '.QIn.Q'][1:], label='QIn')

                plt.plot(plot_time[1:],
                         np.full((len(plot_time[1:]), 1), self.reservoirs[res].properties['q_turbine_max']),
                         '--', color='orange', label='q_turbine_max')
                plt.plot(plot_time[1:],
                         np.full((len(plot_time[1:]), 1), self.reservoirs[res].properties['q_spill_max']),
                         '--', color='green', label='q_spill_max')
                plt.xlabel('Time (days)')
                plt.ylabel('Discharge (m3/s)')
                plt.legend()
                fig1.savefig(os.path.join(self._output_folder, res + '_discharges_simulation.png'))
                plt.clf()
                fig2 = plt.figure(2)
                plt.plot(plot_time[1:], self.get_timeseries(res + '_H')[1:], label="Level")
                plt.plot(plot_time[1:], np.full((len(plot_time[1:]), 1),
                                                self.reservoirs[res].volume_to_level(
                                                    self.reservoirs[res].properties['volume_min'])),
                         '--r', label='minimum volume')
                plt.plot(plot_time[1:], np.full((len(plot_time[1:]), 1),
                                                self.reservoirs[res].volume_to_level(
                                                    self.reservoirs[res].properties['volume_max'])),
                         '--r', label='maximum volume')
                plt.plot(plot_time[1:], np.full((len(plot_time[1:]), 1), self.reservoirs[res].properties['surcharge']),
                         '--', color='orange', label='surcharge')
                plt.plot(plot_time[1:], np.full((len(plot_time[1:]), 1), self.reservoirs[res].properties['fullsupply']),
                         '--', color='green', label='fullsupply')
                plt.legend()
                plt.xlabel('Time (days)')
                plt.ylabel('Level (m)')
                fig2.savefig(os.path.join(self._output_folder, res + '_levels_simulation.png'))
