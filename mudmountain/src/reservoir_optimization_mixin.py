from rtctools.optimization.collocated_integrated_optimization_problem \
    import CollocatedIntegratedOptimizationProblem
from rtctools.optimization.goal_programming_mixin \
    import Goal, StateGoal
from rtctools.optimization.goal_programming_mixin import GoalProgrammingMixin
from rtctools.optimization.timeseries import Timeseries
import os
import logging
from abc import abstractmethod
from typing import Dict
import rtctools.data.csv as csv
import numpy as np
from reservoir import Reservoir

logger = logging.getLogger("rtctools")


class StateRangeGoal(Goal):
    """
    A state goal applies for each state variable and each time step.
    This goal is used to try to keep the state variable in the target range.
    """

    def __init__(self, optimization_problem, priority, variable, target_min=None, target_max=None, weight=1.,
                 function_nominal=1.):
        super().__init__()

        target_count = 0.
        if target_min is not None:
            self.target_min = target_min
            target_count += 1.
        else:
            target_min = 0.
        if target_max is not None:
            self.target_max = target_max
            target_count += 1.
        else:
            target_max = 0.

        assert (target_count > 0)

        self.state = variable
        self.priority = priority
        self.weight = weight
        self.function_range = optimization_problem.bounds()[variable]
        if function_nominal is not None:
            self.function_nominal = function_nominal
        else:
            self.function_nominal = (target_min + target_max) / target_count

    def function(self, optimization_problem, ensemble_member):
        return optimization_problem.state(self.state)

    # The penalty variable is taken to the order'th power.
    order = 1


class UserTimeSeriesRelease(Goal):  # nominal and range are defined in the Modelica model
    def __init__(self, optimization_problem, variable, time, target_min=None, target_max=None, priority=1, order=2,
                 weight=1.0):
        super().__init__()
        self.time = time
        self.priority = priority
        self.variable = variable
        self.order = order
        self.weight = weight
        target_count = 0.
        if target_min is not None:
            self.target_min = target_min
            target_count += 1.
        else:
            target_min = 0.
        if target_max is not None:
            self.target_max = target_max
            target_count += 1.
        else:
            target_max = 0.

        assert (target_count > 0)
        # Every goal needs a rough (over)estimate of the range of the function.
        # We here copy the value range from the .mo file
        self.function_range = optimization_problem.bounds()[variable]
        self.function_nominal = (target_min + target_max) / target_count

    def function(self, optimization_problem, ensemble_member):
        return optimization_problem.state_at(self.variable, self.time)


class StateMinimizationGoal(Goal):
    """
    A state goal applies for each state variable and each time step.
    This goal is used to minimize the state variable.
    """

    def __init__(self, priority, variable, weight=1., function_nominal=1.):
        super().__init__()
        self.state = variable
        self.priority = priority
        self.weight = weight
        self.function_nominal = function_nominal

    def function(self, optimization_problem, ensemble_member):
        return optimization_problem.state(self.state)

    # The penalty variable is taken to the order'th power.
    order = 1


class SmoothQGoal(Goal):
    """
        A state goal applies for each state variable and each time step.
        This smoothing goal is used in combination with the last two constraints to minimize the
        absolute value of the derivative of the flow
    """

    def __init__(self, priority, location, weight=1., function_nominal=1.):
        super().__init__()
        self.state = location
        self.priority = priority
        self.weight = weight
        self.function_nominal = function_nominal

    def function(self, optimization_problem, ensemble_member):
        return optimization_problem.state(self.state)

    order = 1


class ReservoirOptimizationMixin(GoalProgrammingMixin, CollocatedIntegratedOptimizationProblem):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.plot_reservoirs = True

    @property
    @abstractmethod
    def reservoirs(self) -> Dict[str, Reservoir]:
        return {}

    def pre(self):
        # preprocessing is used to process some data before optimization is started.
        super().pre()

        for i in self.reservoirs:
            self.set_timeseries("{}_V".format(self.reservoirs[i].name),
                                self.get_timeseries("{}_V_init".format(self.reservoirs[i].name)))

    # path goals apply for each time step individually

    def _append_path_goals(self, goal, reservoir_name):
        def _isnan(string):
            return string != string

        parameters = self.parameters(0)
        # Todo: parameters should be checked so that only one applies (one true)
        if parameters.get("{}_use_surcharge_max".format(reservoir_name), True):
            _priority = parameters.get("{}_priority_surcharge_max".format(reservoir_name), False)
            goal.append(StateRangeGoal(self, _priority, '{}_V'.format(reservoir_name),
                                       target_max=self.reservoirs[reservoir_name].level_to_volume(
                                           self.reservoirs[reservoir_name].properties['surcharge'])))

        # further reduce maximum reservoir volume to full supply level, if possible
        if parameters.get("{}_use_fullsupply_max".format(reservoir_name), False):
            _priority = parameters.get("{}_priority_fullsupply_max".format(reservoir_name), False)
            goal.append(StateRangeGoal(self, _priority, '{}_V'.format(reservoir_name),
                                       target_max=self.reservoirs[reservoir_name].level_to_volume(
                                           self.reservoirs[reservoir_name].properties['fullsupply'])))
        # Discharge at downstream location for low flow and high flow
        if parameters.get("{}_use_downstreamflow_target_range".format(reservoir_name), False):
            _priority = parameters.get("{}_priority_downstreamflow_target_range".format(reservoir_name), 11)

            if not _isnan(self.reservoirs[reservoir_name].properties['lowflowlocation']):
                goal.append(
                    StateRangeGoal(self, _priority, '{}_Q'.format(self.reservoirs[reservoir_name].properties[
                                                                      'lowflowlocation']),
                                   target_min=parameters.get("{}_downstreamflow_target_min".format(
                                       reservoir_name)),
                                   target_max=parameters.get("{}_downstreamflow_target_max".format(
                                       reservoir_name)),
                                   function_nominal=1e3))

        if parameters.get("{}_use_minimize_Qout".format(reservoir_name), True):
            _priority = parameters.get("{}_priority_minimize_Qout".format(reservoir_name), False)
            goal.append(StateMinimizationGoal(_priority, '{}_Qout'.format(reservoir_name), function_nominal=1e3))

        # Minimization goal of reservoir release via Q_spill
        if parameters.get("{}_use_minimize_Qspill".format(reservoir_name), True):
            _priority = parameters.get("{}_priority_minimize_Qspill".format(reservoir_name), False)
            goal.append(StateMinimizationGoal(_priority, '{}_Qspill'.format(reservoir_name), function_nominal=1e3))
            goal.append(SmoothQGoal(_priority, '{}_absQspillder'.format(reservoir_name)))

        # this goal is used to smooth operation of the turbine over time (and generally given the last priority)
        if parameters.get("{}_use_smoothen_Qturbine".format(reservoir_name), True):
            _priority = parameters.get("{}_priority_smoothen_Qturbine".format(reservoir_name), False)
            goal.append(SmoothQGoal(_priority, '{}_absQturbder'.format(reservoir_name)))

        return goal

    def path_goals(self):

        g = super().path_goals()

        for res in self.reservoirs:
            previous_nrgoals = len(g)
            g = self._append_path_goals(g, res)
            logger.info(
                'Reservoir: {}, Number of goals added: {} for reservoir '.format(res, len(g) - previous_nrgoals))

        return g

    '''
    g = super().path_goals()

    for res in self.reservoirs:
        previous_nrgoals = len(g)
        g = _append_path_goals(g, res)
        logger.info(
            'Reservoir: {}, Number of goals added: {} for reservoir '.format(res, len(g) - previous_nrgoals))

    return g
    '''

    def goals(self):
        goals = super().goals()

        def _append_goals(g, name):

            _priority = self.parameters(0).get("{}_priority_track_release_timeseries".format(name), 10)
            for idx, t in enumerate(self.times()):
                g.append(UserTimeSeriesRelease(variable='{}_Qout'.format(name), time=t,
                                               target_min=self.get_timeseries('{}_Release_User'.format(name)).values[
                                                   idx],
                                               target_max=self.get_timeseries('{}_Release_User'.format(name)).values[
                                                   idx],
                                               order=1, priority=_priority))

            return g

        for res in self.reservoirs:
            previous_nrgoals = len(goals)
            if self.parameters(0).get("{}_use_track_release_timeseries".format(res), True):
                goals = _append_goals(goals, res)
                logger.info(
                    'Reservoir: {}, Number of goals added: {} for reservoir '.format(res,
                                                                                     len(goals) - previous_nrgoals))

        return goals

    def path_constraints(self, ensemble_member):

        def _append_constraints(c, name):
            # set volume constraints
            v_init = self.get_timeseries("{}_V_init".format(self.reservoirs[name].name)).values[0]
            v_max = self.reservoirs[name].properties['volume_max']
            v_min = self.reservoirs[name].properties['volume_min']
            if v_init > v_max:
                logger.error('V_init {} > V_max {}, reservoir {}'.format(v_init, v_max, name))
            if v_init < v_min:
                logger.error('V_init {} < V_min {}, reservoir {}'.format(v_init, v_max, name))

            c.append((self.state('{}_V'.format(name)), v_min, v_max))

            # Constraint for the capacity of the turbines (physical min and max)
            c.append(
                (self.state('{}_Qturbine'.format(name)), 0, float(self.reservoirs[name].properties['q_turbine_max'])))
            # capacity of Q_spill
            c.append((self.state('{}_Qspill'.format(name)), 0, float(self.reservoirs[name].properties['q_spill_max'])))

            # constraints used to minimize the absolute value of the Qturb derivative
            c.append((self.state('{}_absQturbder'.format(name)) - self.der('{}_Qturbine'.format(name)), 0.0,
                      np.inf))
            c.append((self.state('{}_absQturbder'.format(name)) + self.der('{}_Qturbine'.format(name)), 0.0,
                      np.inf))

            # constraints to minimize the absolute value of the Q_spill derivative
            c.append((self.state('{}_absQspillder'.format(name)) - self.der('{}_Qspill'.format(name)), 0.0,
                      np.inf))
            c.append((self.state('{}_absQspillder'.format(name)) + self.der('{}_Qspill'.format(name)), 0.0,
                      np.inf))

            return c

        constraints = super().path_constraints(ensemble_member)

        for res in self.reservoirs:
            constraints = _append_constraints(constraints, res)

        return constraints

    def post(self):

        # postprocessing is used to process some data after optimization has ended.
        # compute water levels from computed volume
        results = self.extract_results()
        for res in self.reservoirs:
            result_h = self.reservoirs[res].volume_to_level(results['{}_V'.format(res)])
            self.set_timeseries('{}_H'.format(res), result_h)
        # End of water level computation
        logging.info("Completed optimization run successfully")
        super().post()

        if self.plot_reservoirs is True:
            import matplotlib.pyplot as plt
            import os
            plt.clf()
            plot_time = [number / (3600 * 24) for number in self.times()]

            def _isnan(string):
                return string != string

            for res in self.reservoirs:
                fig1 = plt.figure(1)
                plt.plot(plot_time[1:], results[res + '_Qturbine'][1:], label='Q_turbine')
                plt.plot(plot_time[1:], results[res + '_Qspill'][1:], label='Q_spill')

                plt.plot(plot_time[1:], self.get_timeseries(res + '_Inflow_Q').values[1:], label='QIn')
                plt.plot(plot_time[1:], self.get_timeseries(res + '_Release_User').values[1:], '--', label='Q_user')
                plt.plot(plot_time[1:],
                         np.full((len(plot_time[1:]), 1), self.reservoirs[res].properties['q_turbine_max']),
                         '--', color='orange', label='q_turbine_max')
                plt.plot(plot_time[1:],
                         np.full((len(plot_time[1:]), 1), self.reservoirs[res].properties['q_spill_max']),
                         '--', color='green', label='q_spill_max')
                plt.xlabel('Time (days)')
                plt.ylabel('Discharge (m3/s)')
                plt.legend()
                fig1.savefig(os.path.join(self._output_folder, res + '_discharges.png'))

                fig2 = plt.figure(2)
                plt.plot(plot_time[1:], self.get_timeseries(res + '_H').values[1:], label="Level")
                plt.plot(plot_time[1:], np.full((len(plot_time[1:]), 1),
                                                self.reservoirs[res].volume_to_level(
                                                    self.reservoirs[res].properties['volume_min'])),
                         '--r')
                plt.plot(plot_time[1:], np.full((len(plot_time[1:]), 1),
                                                self.reservoirs[res].volume_to_level(
                                                    self.reservoirs[res].properties['volume_max'])),
                         '--r')
                plt.plot(plot_time[1:], np.full((len(plot_time[1:]), 1), self.reservoirs[res].properties['surcharge']),
                         '--', color='orange', label='surcharge')
                plt.plot(plot_time[1:], np.full((len(plot_time[1:]), 1), self.reservoirs[res].properties['fullsupply']),
                         '--', color='green', label='fullsupply')
                plt.legend()
                plt.xlabel('Time (days)')
                plt.ylabel('Level (m)')
                fig2.savefig(os.path.join(self._output_folder, res + '_levels.png'))

                if not _isnan(self.reservoirs[res].properties['lowflowlocation']):
                    fig3 = plt.figure(3)
                    plt.plot(plot_time[1:],
                             results['{}_Q'.format(self.reservoirs[res].properties[
                                                       'lowflowlocation'])][1:],
                             label='{}_Q'.format(self.reservoirs[res].properties['lowflowlocation']))
                    plt.plot(plot_time[1:], np.full((len(plot_time[1:]), 1),
                                                    self.parameters(0).get("{}_downstreamflow_target_min".format(res))),
                             '--r')
                    plt.plot(plot_time[1:], np.full((len(plot_time[1:]), 1),
                                                    self.parameters(0).get("{}_downstreamflow_target_max".format(res))),
                             '--r')

                    plt.legend()
                    plt.xlabel('Time (days)')
                    plt.ylabel('Level (m)')
                    fig3.savefig(os.path.join(self._output_folder, res + '_downstream_discharge.png'))

    def solver_options(self):
        options = super().solver_options()
        solver = options['solver']
        options['expand'] = True
        options[solver]['jac_c_constant'] = 'yes'  # equality constraints are linear
        options[solver]['jac_d_constant'] = 'yes'  # inequality constraints are linear
        options[solver]['hessian_constant'] = 'yes'  # nonlinearities (in objective) are at most quadratic

        return options

    @staticmethod
    def write_csv(times, results, output_folder):
        output_variables = ["Downstream_Q", "ReservoirMudMountain_Qout"]
        names = ['time'] + output_variables
        formats = ['O'] + (len(names) - 1) * ['f8']
        dtype = dict(names=names, formats=formats)
        data = np.zeros(len(times), dtype=dtype)
        data['time'] = times
        for output_variable in output_variables:
            try:
                values = results[output_variable]
            except ValueError:
                values = np.full_like(times, -999)
            data[output_variable] = values
        fname = os.path.join(output_folder, 'timeseries_export.csv')
        csv.save(fname, data, delimiter=";", with_time=True)

    def _write_goals_to_csv(self):
        logger.debug("Writing goals for debugging purpose.")
        fname = './debug/goal_output.csv'
        if not os.path.exists(os.path.dirname(fname)):
            os.makedirs(os.path.dirname(fname))
        try:
            o = open(fname, 'w')
        except IOError:
            logger.error('Could not open {}. File locked, no goals written for debug purposes '.format(fname))
        else:
            o.write(
                'goal_type;priority;state;target_min;target_max;frange_min;frange_max;fnominal;\n')
            for g in self.path_goals() + self.goals():
                _min = g.target_min
                _frange_min = g.function_range[0]

                if isinstance(_min, Timeseries):
                    _min = np.min(_min.values)  # Instead of writing the full array, we pick the extreme value.
                elif isinstance(_min, np.ndarray):
                    _min = np.min(_min)  # Instead of writing the full array, we pick the extreme value.
                elif np.isnan(_min):
                    _min = ''
                _max = g.target_max
                _frange_max = g.function_range[1]
                _function_nominal = g.function_nominal
                if isinstance(_max, Timeseries):
                    _max = np.max(_max.values)  # Instead of writing the full array, we pick the extreme value.
                elif isinstance(_max, np.ndarray):
                    _max = np.max(_max)  # Instead of writing the full array, we pick the extreme value.
                elif np.isnan(_max):
                    _max = ''

                o.write('{cname};{priority};{state};{_min};{_max};{_frange_min};{_frange_max};'
                        '{_function_nominal};\n'.format(
                    cname=g.__class__.__name__,
                    _min=_min,
                    _max=_max,
                    _frange_min=_frange_min,
                    _frange_max=_frange_max,
                    _function_nominal=_function_nominal,
                    **g.__dict__))
            o.close()

        _bounds = self.bounds()

        fname = './debug/bounds_output.csv'
        if not os.path.exists(os.path.dirname(fname)):
            os.makedirs(os.path.dirname(fname))
        try:
            o = open(fname, 'w')
        except IOError:
            logger.error('Could not open {}. File locked, no bounds written for debug purposes '.format(fname))
        else:
            for k, (m, M) in _bounds.items():
                o.write("{};{};{}\n".format(k, m, M))
            o.close()
