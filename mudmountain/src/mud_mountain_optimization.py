from rtctools.optimization.collocated_integrated_optimization_problem \
    import CollocatedIntegratedOptimizationProblem
from rtctools.optimization.goal_programming_mixin \
    import GoalProgrammingMixin
from rtctools.optimization.modelica_mixin import ModelicaMixin
from rtctools.optimization.pi_mixin import PIMixin
from rtctools.util import run_optimization_problem
from reservoir_optimization_mixin import ReservoirOptimizationMixin
from reservoir import Reservoir

import logging
import pandas as pd

logger = logging.getLogger("rtctools")


class MudMountainReservoirOptimization(ReservoirOptimizationMixin, GoalProgrammingMixin, PIMixin,
                                       ModelicaMixin, CollocatedIntegratedOptimizationProblem):
    csv_delimiter = ";"

    def __init__(self, *args, reservoirs_csv_path: str, volume_level_csv_paths: str, **kwargs):
        super().__init__(*args, **kwargs)
        self.__debug = False

        res_df = pd.read_csv(reservoirs_csv_path, sep=";", index_col=0)
        vh_data_df = pd.read_csv(volume_level_csv_paths, sep=";", index_col=0)
        self.__reservoirs = {}

        for index, row in res_df.iterrows():
            self.__reservoirs[index] = Reservoir(index, vh_data_df.loc[index], row)
            self.__reservoirs[index].set_volume_setpoints()

    @property
    def reservoirs(self):
        return self.__reservoirs

    def goals(self):
        goals = super().goals()
        return goals

    def path_goals(self):
        goals = super().path_goals()
        return goals

    def path_constraints(self, ensemble_member):
        c = super().path_constraints(ensemble_member)
        if self.__debug:
            self._write_goals_to_csv()
        return c

    def post(self):
        super().post()

        # translate xml file to a csv file
        if self.__debug:
            import pandas4RTC
            pandas4RTC.main()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='RTC_Tools reservoir optimization input arguments.')
    parser.add_argument('--reservoirs_csv_path', help='Path to csv file with reservoir-properties \
    (default: "./../model/reservoirs.csv")', default=r"./../model/reservoirs.csv", nargs='+')
    parser.add_argument('--volume_level_csv_paths', help='List of path to csv files with reservoir volume-level tables \
    (default: "./model/volumelevel.csv")', default=r"./../model/volumelevel.csv", nargs='+')
    parser.add_argument('--solver', help='option to choose solver clp (default) or ipopt as solver', default='clp')

    args = parser.parse_args()
    run_optimization_problem(MudMountainReservoirOptimization, reservoirs_csv_path=args.reservoirs_csv_path,
                             volume_level_csv_paths=args.volume_level_csv_paths, preferred_solver=args.solver)
