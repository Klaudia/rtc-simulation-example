from rtctools.simulation.pi_mixin import PIMixin
from rtctools.simulation.simulation_problem import SimulationProblem
from rtctools.util import run_simulation_problem
import pandas as pd
from reservoir_simulation_mixin import ReservoirSimulationMixin
from reservoir import Reservoir
import logging
import os

logger = logging.getLogger("rtctools")


class MudMountainReservoirSimulation(ReservoirSimulationMixin, PIMixin, SimulationProblem):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__debug = False
        self.Qresults = {}

        # Create reservoirs based on data read from th csv file
        res_df = pd.read_csv(os.path.abspath(kwargs['reservoirs_csv_path'][0]), sep=";", index_col=0)
        vh_data_df = pd.read_csv(os.path.abspath(kwargs['volume_level_csv_paths'][0]), sep=";", index_col=0)
        reservoirs = {}
        for index, row in res_df.iterrows():
            reservoirs[index] = Reservoir(index, vh_data_df.loc[index], row)
            reservoirs[index].set_volume_setpoints()
        reservoirs['ReservoirMudMountain'].fews_location = 'MMRW1'
        reservoirs['ReservoirMudMountain'].fews_qualifier = 'DS'

        self.__reservoirs = reservoirs

    @property
    def reservoirs(self):
        return self.__reservoirs

    def initialize(self, config_file=None):
        # set initial values from the corresponding time series
        for res in self.reservoirs:
            try:
                self.set_timeseries("{}_V".format(self.reservoirs[res].name),
                                    self.get_timeseries("{}_V_init".format(self.reservoirs[res].name)))
            except:
                self.set_timeseries("{}_V".format(self.reservoirs[res].name),self.reservoirs[res].level_to_volume(
                                    self.get_timeseries("{}_H_init".format(self.reservoirs[res].name))))
            self.set_timeseries("{}_Qspill".format(self.reservoirs[res].name),
                                self.get_timeseries("{}_Qspill_init".format(self.reservoirs[res].name)))
            self.set_timeseries("{}_Qturbine".format(self.reservoirs[res].name),
                                self.get_timeseries("{}_Qturbine_init".format(self.reservoirs[res].name)))
        super().initialize(config_file)

    def post(self):
        super().post()
        logging.info("End of Simulation run")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='RTC_Tools reservoir simulation input arguments.')
    parser.add_argument('--reservoirs_csv_path', help='Path to csv file with reservoir-properties \
    (default: ["../model/reservoirs.csv"])', default=[r"../model/reservoirs.csv"], nargs='+')
    parser.add_argument('--volume_level_csv_paths', help='List of path to csv files with reservoir volume-level tables \
    (default: "[./model/volumelevel.csv]")', default=[r"../model/volumelevel.csv"], nargs='+')
    parser.add_argument('--delays_csv_path', help='Path to csv file with delay-properties \
    (default: "./model/delays.csv")', default=[r"../model/delays.csv"], nargs='+')

    args = parser.parse_args()

    run_simulation_problem(MudMountainReservoirSimulation, reservoirs_csv_path=args.reservoirs_csv_path,
                           volume_level_csv_paths=args.volume_level_csv_paths, delays_csv_path=args.delays_csv_path)
