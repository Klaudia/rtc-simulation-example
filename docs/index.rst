.. reservoir-simulation documentation master file, created by
   sphinx-quickstart on Mon Oct 12 13:11:03 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to reservoir-simulation's documentation!
================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   model
   structure



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
