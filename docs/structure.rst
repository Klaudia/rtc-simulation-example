The sturcture of the reservoir model
====================================

Input of information
--------------------

Information is given to the model to several ways:

1. The boundary conditions are given in timeseries_import.xml
2. The initial conditions are given in timeseries_import.xml
3. The reservoir parameters are given in reservoirs.csv
4. The parameters of the model are given in rtcParameterConfig.xml

Calculation
-----------

The simulation model calculates the spilled and turbined flow of the reservoir based on the option the user choses. This option can be chosen in "rtcParameterConfig.xml". The following options are available:

.. highlight:: XML
.. literalinclude:: /../mudmountain/input/rtcParameterConfig.xml
   :lines: 53-55


In this option the outflow is is equal to the inflow and distributed between spillway and turbine flow. First, the water is sent to the turbine, and if the turbine runs with full capacity, the rest of the water is spilled. If the water to spill exceeds the capacity of the spillway, they stay in the reservoir.
The capacity of the turbine and the spill is given at the reservoirs.csv file.

This option allows the user to give a time series for release. This will be distributed between turbine and spill just as explained above.

.. highlight:: XML
.. literalinclude:: /../mudmountain/input/rtcParameterConfig.xml
   :lines: 56-58

If both above options are set false, this option is automatically activated. This gives a constant Q release.

.. highlight:: XML
.. literalinclude:: /../mudmountain/input/rtcParameterConfig.xml
   :lines: 59-61


Note
----
The reservoir bounds have no longer any function.
Note: the volume-level range should be long enough.   








